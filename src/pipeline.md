# How to use

## Sample pipeline for analysis

1. Run nuclear_segment.py on the raw microscope images using a python script (see nuc_segment_run_sample.py for a sample). Save binary masks to a new folder. The program will have you review the masks, so make sure the nuclear boundaries are accurate. Adjust the parameters per image if they are not.

2. Open Matlab, and use openasstack.m to open each channel and the binary masks as 4D images.

3. Feed the images and masks into nucleustrack4.m to obtain trajectory data.

4. Feed each 4D image and nuclear masks into getpuncta4.m to obtain subnuclear puncta binary masks.

5. Run normalize4.m on the 4D images to subtract background for quantitative analysis. Alternatively, open a flat field image and subtract it from the 4D images.

6. Write an analysis script to extract the desired data from the images, using the masks and tracking labels to isolate regions of interest. See analyizepercentpuncta.m for an example script.

7. Analyize result in Matlab, or save as CSV and analyize using a statistical program of choice (Prism, Python, Julia, etc.).

8. Use savestack.m to save subnuclear puncta masks for future use if desired.

## Viewing images

Matlab has a few utilities useful for viewing 3D images: montage, inshow, and volshow.

Imshow will display a 2D image, with monochromatic or RGB pixel values ranging from 0-1, in the Matlab plot GUI.

Montage will take a 3D image matrix, and display each slide as a separate image side-by-side in the Matlab plot GUI.

Volshow will show a 3D image as rendered cubes in a 3D graphics renderer. This is nice for checking the 3D binary masks.

As imshow and montage require pixels to be between 0 and 1, and our images are not guarenteed to meet that, running mat2gray on the image prior to the imview or montage command will normalize them to that range.

In order to reduce the dimensionality of an image (e.g. 4D -> 3D, 3D -> 2D), matrix slicing can be used. E.g., to isolate the first time frame of a 4D image, you can use squeeze(Image(1,:,:,:)). The colons tell Matlab to include all indices along a given dimension.

Here, 4D images are indexed as (Time, X, Y, Z) and 3D images as (X, Y, Z).

Note that in Matlab, indices of a matrix are given using paretheses instead of brackets, and start at 1, not 0.

Examples:
- to visualize a single 3D slice at time n of a 4D image: montage(mat2gray(squeeze(Image(n,:,:,:))))
- to visualize a 3D nuclear mask in 3D: volshow(Mask)

## System Requirements

It is recommended that a modern (4 GHz+) CPU be used, as most fuctions currently execute on the CPU. 32-128 GiB of RAM are recommended, and at least 8-16 are required. For low RAM computers, the functions may be modified to read and write from files, only storing data in RAM as it is being used.
