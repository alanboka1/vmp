# Future plans

- Segmentation should be less user-dependant. Currently, the nuclear and puncta segmentation rely on user-specified parameters. Development is needed to make this process more automatic and robust.
    - Watershed segmentation or edge detection may be helpful.
    - Erosion and dilation might help with separating nearby nuclei.
    - Puncta segmentation has too weak of a criteria for determining puncta currently. It is very noisy.
    - Other image vision techniques may be adapted too.
    - Machine learning approaches may be powerful, but difficult to develop in a robust manner.
- More robust tracking algorithm
    - The Nearest Neighbor tracking algorithm as implemented can lead to tracking errors, biased on separating trajectories that should be the same.
    - Fly nuclei do not exhibit pure diffusion, but have biased movements. An algorithm might be able to take this into account.
    - Perhaps tracking could find neighbors in space and time, rather than only between consecutive time points in a loop.
    - Machine learning approaches might be more possible here, as simulated data to train the algorithms should be relatively easy to produce.
- Improve the computational time of the algorithms
    - Nuclear segmentation in python worked much faster than initial Matlab equivalent.
    - Currently, all Matlab functions are executed on the CPU. GPU computation would significantly speed things up.
    - K-d tree search for tracking might speed things up, as this is currently a major rate limiting step in the pipeline.
