# Thresholding

An easy way to isolate particular regions of an image is by thresholding. In a threshold, a limiting value is selected, and a new image is produced where all pixels above the value are one color and below are another.

While any threshold value may be used, it is often useful to compute an optimal value. The Otsu algorithm tries to find the critical value that partitions a bimodal distribution of intensities into 2 halves. In addition, algorithms have been developed that can give multiple threshold values to separate multi-modal intensity distributions into an arbitrary number of levels, rather than a simple binarization.

In Matlab, graythresh3 will yield the threshold value for Otsu's method on 3D bimodal images, and multithresh will yield a list of threshold values for multi-thresholding. These values can then be used with imbinarize to produce a binary image, or imquantize to produce a leveled image.

As Otsu thresholding requires a clear separation of intensities between the desired object and background, it has difficulty in cases where no clear boundary exists. Looking at an intensity histogram of the data can inform you whether Otsu's algorithm might be useful. In addition, using Gaussian or Median filters on the images prior to thresholding can improve performance.

K-means clustering can also produce images thresholded into an arbitrary number of components. The algorithm clusters pixels into a set number of clusters, such that the intra-cluster variance is minimized. While this algorithm produces a global solution to the same underlying problem as the Otsu threshold, it is significantly slower for a computer to compute.
