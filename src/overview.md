# Volumetric Microscopy Processing

A collection of basic utilities for processing 3D volumetric images of nuclei obtained from fluorescent microscopy. The repository is located [here](https://gitlab.com/alanboka1/vmp).

This repository contains the following programs:

**Python**
- nuclear_segment.py
    - *An image segmentation pipeline for obtaining binary masks of nuclei, with a utility to display the nuclear boundaries and review the segmentation. Saves binary masks as TIF files.*
    - Segmentation parameters: S (sigma of Gaussian 1 for DoG), K (ratio of sigma2/sigma1), min_size (minimum size of nucleus)
- nuc_segment_run_sample.py
    - *A sample script to run the nuclear_segment.py utility on a collection of images. Assumes images are stored with Z-stacks in individual files in TIF format, in a separate directory for each color channel.*

**Matlab**
- openstack.m
    - *Opens TIF file containing multiple image file directories as a 3D single precision floating point matrix. Assumes each image contains the same pixel width/height, and contains monochromatic pixel data. The index order of the matrix corresponds to the (X,Y,Z) coordinates of a voxel.*
- openasstack.m
    - *Opens series of TIF images as a 4D matrix, sorted alphanumerically by filename. Calls selectimages.m and openstack.m. The index order of the matrix corresponds to the (T,X,Y,Z) coordinates of a voxel.*.
- savestack.m
    - *Saves 3D or 4D matrix as monochromatic TIF image. If 3D, the image is stored in a single TIF file, with each Z slice (3rd dimension of matrix) stored in a separate TIF image file directory. If 4D, each 3D slice is stores in a separate TIF file.*
- maxproject.m
    - *Takes a 3D image as input, and returns a 2D image with each pixel corresponding to the maximum voxel intensity at position (X,Y) along the Z dimension.*
- integrate_intensity.m
    - *Computes the sum of all pixels in a masked region. Allows for 2 masks to be input, and will only count pixels included in both. To use with only 1 mask, use '1-zeros(size(3d image))' as the second mask.*
    - Inputs: 3D image, 3D binary mask, additional 3D mask.
    - Outputs: [total intensity, numer of pixels counted]
- normalize4.m
    - *Uses Gaussian background estimation to normalize a 4D volumetric image against background illumination gradients.*
- findmaxima3.m
    - *Identified local maxima pixels in a 3D volumetric image within a masked region. Allows for a radius of exclusion, within which multiple maxima are prohibited. If 2 or more local maxima are within the radius distance, only the most intense will be counted. Also allows for setting an intensity threshold, ignoring voxels that do not meet the threshold.*
    - Inputs: 3D image, mask, radius of exclusion (in pixel units), intensity threshold
    - Outputs: Nx3 matrix of (X,Y,Z) coordinates of each maxima.
- nucleustrack4.m
    - *Naive O(N^4) Nearest-Neighbor tracking algorithm for nuclei in 4D image. Identifies individual nuclei, computes their center of mass, and links nuclei together across time. Allows for specifying a maximum distance a nucleus can travel, above which nuclei will be considered separate.*
    - Inputs: 4D binary mask image, maximum distance between frames
    - Outputs: 4D label matrix image, with each nucleus assigned a unique number.
- getPuncta.m
    - *Performs a simple segmentation pipeline to generate binary masks of subnuclear puncta. Uses multi-thresholding, where the highest threshold is uses to generate a binary mask.*
    - Inputs: 3D image, 3D binary mask of nuclei, number of thresholds, minimum puncta size
    - Outputs: 3D binary mask of puncta
- getpuncta4.m
    - *Calls getPuncta.m on 4D volumetric image*
- analyizepercentpuncta.m
    - *Sample analysis pipeline to integrate the fluorescent signal within subnuclear puncta across 2 channels in 4D data.*
    - Inputs: 4D image of channel 1, 4D image of channel 2, puncta mask of channel 1, puncta mask of channel 2, trajectory label matrix (also serves as nucleus mask)
    - Outputs: Nx8 matrix containing the data: Time, Nucleus #, Ch1 intensity in puncta, Ch1 intensity outside puncta, Ch1 intensity in puncta shared by both channels, Ch2 intensity in puncta, Ch2 intensity outside puncta, Ch2 intensity in puncta shared by both channels