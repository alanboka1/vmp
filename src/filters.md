# Overview of Image Filters

## Convolutions and correlations

Many image filters are implemented as a convolution of a filter matrix (the kernel) with the image data. 

A convolution computes the sum of the products of an image's pixels with the value of the kernel matrix centered at a given pixels, and sets the resultant pixel in the final image to that value. In essence, a kernel sets a pixel's value to the average of all surrounding pixels, weighted by the kernel matrix.

As the kernel and image are not infinite domain functions, the algorithms must be instructed how to handle boundary cases: typically the kernel matrix will assume a value of 0 outside of its boundary, and the image will either be set to 0 or the value of the closest pixel inside the image boundary.

While convolutions are often implemented as a modified discrete Fast Fourier Transform under the hood, many programming languages including Matlab and Python contain libraries which provide simple interface functions for image convolution, requiring only a kernel matrix and an image.

The math behind convolutions can be found on [Wikipedia](https://en.wikipedia.org/wiki/Convolution).

In addition, many image processing libraries also use correlation functions to perform filtering. Correlations measure the similarity between 2 functions (matrices), such as in the case of autocorrelation functions. Both convolutions and correlations produce similar outcomes when used for image filtering, and in the case that the kernel is rotationally symmetric (as is the case for Gaussian kernels), the output is identical.

In Matlab, imfilter will filter an arbitrary dimensional image with an arbitrary kernel matrix. The user can specify whether to use convolutions or correlations, and how to handle boundary cases.

## Gaussian filter
A Gaussian filter uses a kernel with values defined by a Gaussian function centered at the central element of the kernel matrix. A Gaussian function can be defined for any dimension of image.

In essence, the Gaussian filter sets a pixel value to the average of all other pixels, weighted by the distance from the pixel in question. The result of this is a blurring of high spatial frequency features (e.g. subtle details, noise, edges) while leaving low spatial frequency features (e.g. large objects, illumination gradients) basically untouched.

As a result, the Gaussian filter acts as a spatial low pass filter on an image. A similar effect can be obtained by taking the FFT of the image, removing low frequency peaks, and taking the inverse FFT.

The main parameter of a Gaussian filter is the sigma of the Gaussian function (equivalent to standard deviation, in the context of statistics). A larger sigma results in more drastic blurring of lower frequencies.

Gaussian filters are often useful prior to image thresholding in order to combat against noise. This makes them really useful as a first step in segmenting large objects from an image.

In addition, a Gaussian filter with a large sigma can be used to estimate the background illumination gradient in fluorescent microscopy images. While this is not a fully rigorous method, if background subtraction is needed but a flat field image was not acquired, this can be a useful technique.

## Difference of Gaussians: Blob detection
A common requirement in image analysis is detecting objects in an image. The difficulty is that a computer has no idea what an "object" is, so we have to come up with a way of telling it. As computers only understand basic arithmetic and conditional logic, a mathematical algorithm for finding objects without any conscious insight is needed.

One approach is known as the Difference of Gaussians (DoG) filter. Two Gaussian filters are obtained for an image: one with a larger sigma than the other. Then, the larger Gaussian filtered image is subtracted from that of the smaller.

Because Gaussian filters act as low pass filters, the first Gaussian will blur out high frequency data. Then, the second Gaussian will blur high and lower frequency data. The difference of the two will only contain features whose spatial frequency is lower than that of the first, but higher than that of the second. As a result, this filter acts as a spatial mid pass filter.

By optimizing the sizes of the two sigmas to the sizes of the objects you want to detect, the resulting difference of Gaussians image will blur out objects significantly larger or smaller than those desired. Then, a threshold can be applied to create a mask of the desired objects.
