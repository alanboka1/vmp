# Summary

- [Overview](./overview.md)
- [Filters](./filters.md)
- [Thresholds](./thresholding.md)
- [Tracking](./tracking.md)
- [How to Use](./pipeline.md)
- [Future directions](./future.md)