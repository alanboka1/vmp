# Tracking algorithms

The goal of tracking algorithms is to take a list of coordinates (in 2 or 3 dimensions across time), and determine which coordinates likely correspond to the same object. To humans, this is usually easy as our brains predict motion and track objects automatically, but a computer needs to be told how to do this.

## Nearest Neighbor
One of the simplest tracking algorithms is nearest neighbor tracking. The idea is that two objects which are very close between 2 consecutive time points are likely the same object.

First, 2 consecutive time points are selected. Next, a pair-wise distance matrix is computed, giving the distance between each coordinate at time 1 and time 2 (the square of the distance is used in practice because a square root is not needed for the algorithm logic, and square roots are VERY computationally expensive).

Next, for each coordinate at time 1, the nearest coordinate at time 2 is selected and assigned as the nearest neighbor.

This is then repeated for all coordinates at time 1, being careful not to assign a single coordinate as the nearest neighbor of 2 coordinates. Any unassigned coordinates at time 2 become the start of a new trajectory.

The result of this is the trajectory data of all objects.

To make tracking errors less likely, a maximum distance between nearest neighbors can be specified.

However, this algorithm has several downsides:
- It selects neighbors in an arbitrary order, and once a neighbor is selected it is final. This means that it can only find a locally optimal solution, and better trajectories may exist.
- The algorithm takes O(N^M) computational time, where M is the number of dimensions including time in the data. This can be improved by using a k-d tree search instead of simple loops, although this is often unneeded if only a few objects are being tracked.

## Other algorithms

Other tracking algorithms exist, such as the Hungarian pairing algorithmn, Kalman filtering, and min cost flow algorithms. The selection of a tracking algorithm must be made based on the required computational complexity, ease of development and maintenance, size of data, and assumed models of motion.