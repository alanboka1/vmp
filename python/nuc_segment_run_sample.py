#Segments nuclei from 3D TIF stack using DoG and Otsu threshold
#Dependencies: python3, scikit-image, numpy, scipy
#To run: enter paths of image channels, and output directory (assumes channels are in separate directory)
#        enter desired initial segmentation parameters (S, K, Min_Size)
#        open shell and run "python [RUN_SCRIPT_FILENAME].py"

from pathlib import Path
from nuclear_segment import *

matplotlib.use("tkagg") #can remove if not on MacOS

if __name__ == "__main__":
    #Data directories
    Channel_1_Directory = "./hp1/"
    Channel_2_Directory = "./gaf/"
    Mask_Out_Directory  = "./msk/"
    
    #Default segmentation parameters
    S_Default = 2.5 #initial radius for DoG
    K_Default = 6.5 #ratio of DoG radii
    Min_Size_Default = 100 #minimum size of nuclei
    
    #Load list of files
    list1 = os.listdir(Channel_1_Directory)
    list2 = os.listdir(Channel_2_Directory)
    list1.sort()
    list2.sort()
    
    ch1fn = [Channel_1_Directory + e for e in list1]
    ch2fn = [Channel_2_Directory + e for e in list2]
    mskfn = [Mask_Out_Directory + str(i) + ".tif" for i in range(len(ch1fn))]
    
    #Run initial segmentation
    v = []
    Path(Mask_Out_Directory).mkdir(exist_ok=True)
    for inx in range(len(ch1fn)):
        v.append(createImage3D(ch1fn[inx], ch2fn[inx], mskfn[inx], S_Default, K_Default, Min_Size_Default))
    
    #Show GUI for segmentation review
    R = Review4DSet(v)
    R.show()
