import os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.widgets as pwidget
import skimage.filters as ifilt
import skimage.morphology as imorph
import skimage.segmentation as iseg

from skimage.exposure import equalize_hist
from scipy import ndimage
from skimage.io import imread, imsave

#Data class to store 2-channel 3D image and mask
class Image3D:
    def __init__(self, ch1, ch2, mask):
        self.ch1_path = ch1
        self.ch2_path = ch2
        self.mask_path = mask
        self.S = 0
        self.K = 0
        self.Min = 0
        self.I1 = []
        self.I2 = []
        self.M = []
        self.changed = True
        self.slices = 0
        self.view_inx = 0
    def openimg(self):
        if self.ch1_path != "":
            self.I1 = imread(self.ch1_path)
            self.slices = self.I1.shape[0]
        if self.ch2_path != "":
            self.I2 = imread(self.ch2_path)
    def calculate(self):
        if self.changed:
            self.M = nuclearsegment3(self.I1, self.S, self.K, self.Min)
            imsave(self.mask_path, self.M)
            self.changed = False
    def load_params(self, s, k, min_size):
        if self.S != s or self.K != k or self.min_size != min_size:
            self.changed = True
            self.S = s
            self.K = k
            self.Min = min_size

#Provides interface to view image stacks with nuclear mask, and update segmentation parameters
class Review4DSet:
    def __init__(self, v):
        self.img_v = v
        self.img_ptr = 0
        self.fig, self.ax = plt.subplots()
        self.fig.canvas.mpl_connect('scroll_event', self._scroll_callback)
        plt.axis('off')
        self.I = np.zeros(self.img_v[0].I1.shape)
        self.im = self.ax.imshow(self.I[0,:,:], cmap="gray")
        self.change_buffer = True
        
        #Widget elements
        self.ax1 = plt.axes([0.1,  0.1, 0.08, 0.05])
        self.ax2 = plt.axes([0.22, 0.1, 0.08, 0.05])
        self.ax3 = plt.axes([0.1,  0.2, 0.2, 0.05])
        self.ax4 = plt.axes([0.2,  0.3, 0.1, 0.05])
        self.ax5 = plt.axes([0.2,  0.4, 0.1, 0.05])
        self.ax6 = plt.axes([0.2,  0.5, 0.1, 0.05])
        
        self.back_button = pwidget.Button(self.ax1, "Prev")
        self.next_button = pwidget.Button(self.ax2, "Next")
        self.calc_button = pwidget.Button(self.ax3, "Recalculate")
        self.min_box = pwidget.TextBox(self.ax4, "Min Size:")
        self.k_box = pwidget.TextBox(self.ax5, "K:")
        self.s_box = pwidget.TextBox(self.ax6, "S:")
        
        self.back_button.on_clicked(self._back_callback)
        self.next_button.on_clicked(self._next_callback)
        self.calc_button.on_clicked(self._calc_callback)
    def show(self):
        self.update_img()
        plt.show()
    def update_img(self):
        self.s_box.set_val(str(self.img_v[self.img_ptr].S))
        self.k_box.set_val(str(self.img_v[self.img_ptr].K))
        self.min_box.set_val(str(self.img_v[self.img_ptr].Min))
        if self.change_buffer:
            self.I = pseudocolor3(self.img_v[self.img_ptr].I1, self.img_v[self.img_ptr].I2, self.img_v[self.img_ptr].M)
            self.change_buffer = False
        self.im.set_data(self.I[self.img_v[self.img_ptr].view_inx,:,:])
        self.ax.set_title('Image %s, Slice %s/%s' % (self.img_ptr+1, self.img_v[self.img_ptr].view_inx+1, self.img_v[self.img_ptr].slices+1))
        self.im.axes.figure.canvas.draw()
    def _scroll_callback(self, event):
        if event.button == 'up':
            self.img_v[self.img_ptr].view_inx = min(self.img_v[self.img_ptr].view_inx + 1, self.img_v[self.img_ptr].slices-1)
        else:
            self.img_v[self.img_ptr].view_inx = max(self.img_v[self.img_ptr].view_inx - 1, 0)
        self.update_img()
    def _back_callback(self, event):
        self.img_ptr = max(0, self.img_ptr - 1)
        self.change_buffer = True
        self.update_img()
    def _next_callback(self, event):
        self.img_ptr = min(len(self.img_v)-1, self.img_ptr + 1)
        self.change_buffer = True
        self.update_img()
    def _calc_callback(self, event):
        self.img_v[self.img_ptr].load_params(float(self.s_box.text), float(self.k_box.text), int(self.min_box.text))
        self.img_v[self.img_ptr].calculate()
        self.change_buffer = True
        self.update_img()

#Create 2-color pseudocolor image of channels, and outline nucleus boundaries
def pseudocolor3(channel1, channel2, m, z_project=False):
    if z_project:
        ch1 = maxproject(channel1)
        ch2 = maxproject(channel2)
        mask = maxproject(m)
    else:
        ch1 = channel1
        ch2 = channel2
        mask = m
    boundary = np.zeros(mask.shape)
    for inx in range(len(mask)):
        boundary[inx, :, :] = iseg.find_boundaries(mask[inx, :, :], connectivity=1, mode='thick', background = 0)
        
    I = np.zeros(list(ch1.shape) + [3])
    I[:,:,:,0] = equalize_hist(ch1) * (1-boundary.astype(np.float_))
    I[:,:,:,1] = equalize_hist(ch2) * (1-boundary.astype(np.float_))
    I[:,:,:,2] = boundary.astype(np.float_)
    return I

#Calculate 3D nucleus boundary mask using DoG/Otsu threshold
def nuclearsegment3(image, s, k, min_size):
    m = ifilt.difference_of_gaussians(image, s, s*k)
    t = ifilt.threshold_multiotsu(m, classes=2)
    m = (m > t).astype(np.bool_)
    m = imorph.remove_small_objects(m, min_size=min_size, connectivity=1)
    m = ndimage.binary_fill_holes(m, structure=np.ones((1,3,3)).astype(np.bool_))
    return m

def createImage3D(ch1, ch2, mask, s, k, min_size):
    I = Image3D(ch1, ch2, mask)
    I.openimg()
    I.load_params(s,k,min_size)
    I.calculate()
    return I
