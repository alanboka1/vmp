# Basic Volumetric Microscopy Toolkit

Toolkit of Python scripts and MATLAB routines for volumetric image processing of 4D nucleus fluorescent microscopy data.

**Dependencies:** MATLAB (w/ image processing toolkit); Python 3+ (w/ scikit-image, scipy, numpy); [natsort (for MATLAB)](https://www.mathworks.com/matlabcentral/fileexchange/47434-natural-order-filename-sort)

Documentation: [https://alanboka1.gitlab.io/vmp/](https://alanboka1.gitlab.io/vmp/)

**Files:**

```
python/nuclear_segment.py          - functions for segmenting nuclei in 3D datasets
python/nuc_segment_run_sample.py   - sample script for passing data to "nuclear_segment.py"
matlab/openstack.m                 - opens TIF stack file as 3D image
matlab/openasstack.m               - opens series of TIF files as 4D image
matlab/savestack.m                 - saves 3D image as TIF file, or 4D image as series of TIF files
matlab/selectimages.m              - returns sorted array of filenames 
matlab/maxproject.m                - performs a maximum projection on 3D image
matlab/integrate_intensity.m       - integrates intensity of pixels in image, within a masked region
matlab/normalize4.m                - performs normalization on 4D image to correct against estimated local background
matlab/findmaxima3.m               - finds maxima values in 3D image, with radius of exclusion for neighboring maxima
matlab/nucleustrack4               - performs nearest-neighbor tracking on 4D binary mask of nuclear volumes
matlab/getPuncta.m                 - segments intra-nuclear puncta within a 3D image, given a binary mask of nuclear volumes
matlab/getpuncta4.m                - runs getPuncta.m on 4D image
matlab/analyizepercentpuncta.m     - runs analysis of signal in/out of puncta in single nuclei, given 2 channels of a 4D image, nuclear masks, and nuclear trajectories
```
