function mp = maxproject(image)
s = size(image);
mp = zeros(s(1), s(2));

for i = 1:s(1)
    for j = 1:s(2)
        mp(i,j) = max(image(i,j,:));
    end
end
end

