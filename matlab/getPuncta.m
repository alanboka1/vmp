function img = getPuncta(img_in,mask, n, min_size)
kernel = zeros(3,3,3);
kernel = -1+kernel;
kernel(2,2,2) = 26;

%background subtraction
bg = imgaussfilt3(img_in, 30);
img = img_in - bg;

%DoG
img2 = imgaussfilt3(img, 1);
img = img2 - imgaussfilt3(img, 3);

img = imfilter(img, kernel);

%normalize intensity to [0,1]
img = mat2gray(img);

%apply mask
img(mask == 0) = nan;

t = multithresh(img, n);

img = imbinarize(img, t(n));
img = bwareaopen(img, min_size, 8);
end

