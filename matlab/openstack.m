function im3d = openstack(filename)
%Opens TIF stack representing 3D image and returns
% single-precision floating point HEIGHTxWIDTHxDEPTH matrix
metadata = imfinfo(filename);
im = imread(filename, 1);
for n = 2:size(metadata, 1)
    im = cat(3, im, imread(filename, n));
end
im3d = single(im);
end