function maxima = findmaxima3(image, mask, radius, threshold)
%Finds maxima in 3D Image.
%Excludes multiple maxima within radius,
% and maxima below intensity threshold.
%Inputs: 3D monochromatic image (ideally normalized to [0,1]), 
%        binary mask of same dimensions, 
%        radius, threshold.
%Output: Matrix of coordinates of peaks (Nx3)

N_maxima = 0;

maxima_x = zeros(1, 100);
maxima_y = zeros(1, 100);
maxima_z = zeros(1, 100);

S = size(image);

%Iterate through voxels
for i = 2:S(1)-1
    for j = 2:S(2)-1
        for k = 2:S(3)-1
            %Check voxel is not masked out
            if mask(i,j,k) == 0
                continue;
            end
            v = image(i,j,k);
            %Check voxel meets intensity threshold
            if v < threshold
                continue;
            end
            %Check voxel is maximum in immediate neighborhood
            for k2 = k-1:k+1
            if ~(v >= image(i,j-1,k2) && v >= image(i,j+1,k2) && v >= image(i-1,j,k2) ...
                      && v >= image(i+1,j,k2) && v >= image(i-1,j-1,k2) && v >= image(i-1,j+1,k2) ...
                      && v >= image(i+1,j-1,k2) && v >= image(i+1,j+1,k2))
                  continue;
            end
            end
            add_new = true;
            %Check maxima with higher intensity not in radius
            for n = 1:N_maxima
                dx = abs(maxima_x(n) - i);
                dy = abs(maxima_y(n) - j);
                dz = abs(maxima_z(n) - k);
                %See if maxima is within radius of new maxima
                if dx <= radius && dy <= radius && dz <= radius
                    add_new = false;
                    %If new maxima is greater intensity, replace old
                    if image(maxima_x(n), maxima_y(n), maxima_z(n)) < v
                        maxima_x(n) = i;
                        maxima_y(n) = j;
                        maxima_z(n) = k;
                        break;
                    end
                end
            end
            %If no existing maxima in radius, add new maxima to list
            if add_new
                %Reallocate arrays if needed
                if length(maxima_x) < N_maxima + 1
                    maxima_x = [maxima_x zeros(1,N_maxima)];
                    maxima_y = [maxima_y zeros(1,N_maxima)];
                    maxima_z = [maxima_z zeros(1,N_maxima)];
                end
                maxima_x(N_maxima+1) = i;
                maxima_y(N_maxima+1) = j;
                maxima_z(N_maxima+1) = k;
                N_maxima = N_maxima + 1;
            end
        end
    end
end

maxima = [maxima_x(1:N_maxima); maxima_y(1:N_maxima); maxima_z(1:N_maxima)]';
end

