function puncta = getpuncta4(raw_img,masks, n)
%INPUT = 4D image stack (time, x, y, z)
%OUTPUT = 4D image stack of puncta
puncta = zeros(size(raw_img));
for t = 1:size(raw_img, 1)
    raw(:,:,:) = raw_img(t,:,:,:);
    mask(:,:,:) = masks(t, :, :, :);
    puncta(t,:,:,:) = getPuncta(raw, mask, n, 8);
end
end

