function data = analyzepercentpuncta(ch1, ch2, puncta1, puncta2, trajectories)
%INPUT: normalized images (4Dx2), puncta images (4Dx2), nuclear
%trajectories
%OUTPUT: Kx8 matrix, with each row containing:
% [TIME] [NUCLEUS] [CH1-IN] [CH1-OUT] [CH1-COLOC-IN] [CH2-IN] [CH2-OUT] [CH2-COLOC-IN]

data = [];

for t = 1:size(ch1, 1) %iterate over time points
    nuclei_values = unique(trajectories(t, :, :, :));
    for nucleus_inx = 2:length(nuclei_values) %skip index 1 = zeros
        I1(:,:,:) = mat2gray(ch1(t,:,:,:));
        I2(:,:,:) = mat2gray(ch2(t,:,:,:));
        PM1(:,:,:) = logical(puncta1(t,:,:,:));
        PM2(:,:,:) = logical(puncta2(t,:,:,:));
        NucMsk = zeros(size(ch1, 2), size(ch1, 3), size(ch1, 4));
        NucMsk(:,:,:) = trajectories(t,:,:,:);
        NucMsk = (NucMsk == nuclei_values(nucleus_inx));
        intensity_1_in = integrate_intensity(I1, NucMsk, PM1);
        intensity_1_out = integrate_intensity(I1, NucMsk, 1-PM1);
        intensity_2_in = integrate_intensity(I2, NucMsk, PM2);
        intensity_2_out = integrate_intensity(I2, NucMsk, 1-PM2);
        intensity_1_coloc = integrate_intensity(I1, NucMsk, PM1 .* PM2);
        intensity_2_coloc = integrate_intensity(I2, NucMsk, PM1 .* PM2);
        data(length(data)+1,:) = [t nuclei_values(nucleus_inx) intensity_1_in intensity_1_out intensity_1_coloc intensity_2_in intensity_2_out intensity_2_coloc];
        %using length(data)+1 creates set of 0s after first row of matrix when matrix is 1x8 (i assume it think length=8 due to other dimension being 1), look into better metric
    end
end
end

