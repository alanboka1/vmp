function savestack(filename,img)
S = size(img);
DIMS = length(S);
if DIMS == 3
    imwrite(img(:,:,1), filename, 'tif');
    D = S(3);
    for i = 2:D
        imwrite(img(:,:,i), filename, 'tif', 'WriteMode', 'append');
    end
elseif DIMS == 4
    for t = 1:S(1)
        I(:,:,:) = mat2gray(img(t, :, :, :));
        imwrite(I(:,:,1), strcat(filename, string(t), ".tif"), 'tif');
        D = S(4);
        for i = 2:D
            imwrite(I(:,:,i),strcat(filename, string(t), ".tif"), 'tif', 'WriteMode', 'append');
        end
    end
elseif DIMS == 1
    imwrite(img, filename);
end
end

