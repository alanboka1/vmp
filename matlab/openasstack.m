function Images = openasstack()
filenames = selectimages();
Images = [];
for i = 1:length(filenames)
    Images(i,:,:,:) = openstack(filenames(i));
end
end