function [Trajectories, N_trajectories] = nucleustrack4(masks, max_distance)
%nucleustrack3 uses frame-by-frame nearest neighbor tracking to generate
% nuclear trajectories from 4D volumetric nuclear masks.
% 
% INPUTS = TxNxMxK binary matrix, maximum distance between nearest
% neighbors. If max_distance is 0, no max distance will be assigned.
%
% OUTPUTS = TxNxMxK matrix, where 0 corresponds to non-nuclear voxels, and
% other data indicates the trajectory # of nuclear voxels.
S = size(masks);
Timepoints = S(1);
DimX = S(2);
DimY = S(3);
DimZ = S(4);

Trajectories = zeros(Timepoints, DimX, DimY, DimZ);

Labeled_Nuclei = zeros(Timepoints, DimX, DimY, DimZ);
Nuclei_Counts  = zeros(Timepoints, 1);
Points = [];

%Assign nuclei and get centers
for i = 1:Timepoints
    [Labeled_Nuclei(i, :, :, :), Nuclei_Counts(i, 1)] = bwlabeln(masks(i, :, :, :));
    Points = [Points; centers3(Labeled_Nuclei(i, :, :, :), i)];
end

%Nearest Neighbor
N_trajectories = Nuclei_Counts(1); %set initial trajectory count to # of nuclei
Trajectories(1, :) = Labeled_Nuclei(1, :); %set initial frame of output to initial frame of labels
for t = 2:Timepoints
    P1 = Points(Points(:,4)==t-1,:);
    P2 = Points(Points(:,4)==t,:);
    D = getdistances(P1, P2, max_distance);
    IDs = zeros(1,Nuclei_Counts(t));
    for nuclear_inx = 1:Nuclei_Counts(t) %Iterate over nuclei of second frame
        [min_dist, nn_inx] = min(D(:,nuclear_inx), [], 'linear');
        if isinf(min_dist)
            IDs(nuclear_inx) = N_trajectories + 1;
            N_trajectories = N_trajectories + 1;
        else
            IDs(nuclear_inx) = Trajectories(t-1, floor(P1(nn_inx, 1)), floor(P1(nn_inx, 2)), floor(P1(nn_inx, 3)));
            D(nn_inx,:) = Inf;
        end
        Trajectories(t, Labeled_Nuclei(t,:,:,:) == nuclear_inx) = IDs(nuclear_inx);
    end
end
end

function Centers = centers3(labeled_mask, t)
N = max(labeled_mask(:));
X = zeros(1, N);
Y = zeros(1, N);
Z = zeros(1, N);
Counts = zeros(1, N);
%Calculate average coordinates
for i = 1:size(labeled_mask, 2)
    for j = 1:size(labeled_mask, 3)
        for k = 1:size(labeled_mask, 4)
            if labeled_mask(1,i,j,k) == 0
                continue
            end
            X(labeled_mask(1,i,j,k)) = X(labeled_mask(1,i,j,k)) + i;
            Y(labeled_mask(1,i,j,k)) = Y(labeled_mask(1,i,j,k)) + j;
            Z(labeled_mask(1,i,j,k)) = Z(labeled_mask(1,i,j,k)) + k;
            Counts(labeled_mask(1,i,j,k)) = Counts(labeled_mask(1,i,j,k)) + 1;
        end
    end
end

X = X ./ Counts;
Y = Y ./ Counts;
Z = Z ./ Counts;

Centers(:,1) = X;
Centers(:,2) = Y;
Centers(:,3) = Z;
Centers(:,4) = zeros(1, N) + t;
end

function Distances = getdistances(coords1, coords2, max_distance)
N_coords1 = size(coords1,1);
N_coords2 = size(coords2,1);
Distances = zeros(N_coords1, N_coords2);

for i = 1:N_coords1
    for j = 1:N_coords2
        Distances(i, j) = (coords1(i, 1) - coords2(j, 1))^2 + (coords1(i, 2) - coords2(j, 2))^2 + (coords1(i, 3) - coords2(j, 3))^2;
        if max_distance ~= 0 && Distances(i,j) > max_distance
            Distances(i,j) = Inf;
        end
    end
end
end
