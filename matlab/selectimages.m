function files = selectimages()
[files, path] = uigetfile({'*.tif'; '*.tiff'}, 'MultiSelect', 'on');
for inx = 1:length(files)
    files(inx) = strcat(path, files(inx));
end
files = string(natsortfiles(files));
end