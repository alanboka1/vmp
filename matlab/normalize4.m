function out = normalize4(img)
%Normalize 4D image (time, x, y, z) to estimated local background
out = zeros(size(img));

for t = 1:size(img, 1)
    I(:,:,:) = img(t, :, :, :);
    %background subtraction
    bg = imgaussfilt3(I, 30);
    I = I - bg;
    out(t, :, :, :) = I;
end
end

