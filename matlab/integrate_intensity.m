function [intensity, N] = integrate_intensity(img,nuclear_mask, mask)
data = img(nuclear_mask .* mask ~= 0);
N = length(data);
intensity = sum(data);
end

